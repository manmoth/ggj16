﻿using UnityEngine;
using System.Collections;

public class NetworkMessageConstants {

	// Message types
	public const short MSG_TYPE_MOVE 	= 0x0F10;
	public const short MSG_TYPE_PAIRING	= 0x0F20;
	public const short MSG_TYPE_NICK	= 0x0F30;

	// Move
	public const short ARG_MOVE_STOP 	= 0x0F11;
	public const short ARG_MOVE_LEFT 	= 0x0F12;
	public const short ARG_MOVE_RIGHT 	= 0x0F13;

	// Pairing
	public const short ARG_PAIRING_DONE	= 0x0F21;
	public const short ARG_PAIRING_END	= 0x0F22;
}
