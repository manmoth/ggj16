﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuCanvas : MonoBehaviour {

	public MasterScript masterScript;

	// Use this for initialization
	void Start () {
		InputField[] inputFields = transform.GetComponentsInChildren<InputField> ();
		inputFields [0].text = "Baby" + Random.Range(1000, 9999);
		inputFields [1].text = "129.177.32.11";
		inputFields [2].text = "8487";
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey ("escape")) {
			Application.Quit ();
		}
	}

	public void ConnectButtonClicked() {
		InputField[] inputFields = transform.GetComponentsInChildren<InputField> ();
		string nick = inputFields [0].text;
		string host = inputFields [1].text;
		string port_string = inputFields [2].text;
		int port;
		if (System.Int32.TryParse (port_string, out port)) {
		} else {
			Debug.LogError ("String could not be parsed.");
			return;
		}

		masterScript.nick = nick;
		masterScript.host = host;
		masterScript.port = port;

		SceneManager.LoadScene ("BabyMote");
	}

	public void StartButtonClicked() {
		
	}

}
