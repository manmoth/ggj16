﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BabyMoteCanvas : MonoBehaviour {

	private MasterScript masterScript;
	private NetworkClient client;

	private GameObject waitPanel;

	int current_pointer_id;
	int current_direction;
	int prev_pointer_id;
	int prev_direction;

	// Use this for initialization
	void Start () {
		current_pointer_id = -1;
		current_direction = 0;
		prev_pointer_id = -1;
		prev_direction = 0;

		waitPanel = GameObject.Find ("waitpanel");

		masterScript = GameObject.FindObjectOfType<MasterScript> ();

		transform.GetComponentInChildren<Text> ().text = "Nick: " + masterScript.nick;

		client = new NetworkClient();

		string host = masterScript.host;
		int port = masterScript.port;
		client.Connect(host, port);

		// system msgs
		client.RegisterHandler(MsgType.Connect, OnClientConnect);
		client.RegisterHandler(MsgType.Disconnect, OnClientDisconnect);
		client.RegisterHandler(MsgType.Error, OnClientError);

		client.RegisterHandler(NetworkMessageConstants.MSG_TYPE_PAIRING, OnPairingMessage);

		DontDestroyOnLoad(gameObject);	
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKey ("escape")) {
			Application.Quit ();
		}
	}

	void OnClientConnect(NetworkMessage netMsg) {
		Debug.Log("Client Connected to Master");
		StringMessage msg = new StringMessage (masterScript.nick);
		Debug.Log ("Client sendt nick '" + masterScript.nick + "' to server");
		client.Send (NetworkMessageConstants.MSG_TYPE_NICK, msg);

	}

	void OnClientDisconnect(NetworkMessage netMsg) {
		Debug.Log("Client Disconnected from Master");
	}

	void OnClientError(NetworkMessage netMsg) {
		Debug.Log("ClientError from Master");
	}

	void OnPairingMessage(NetworkMessage netMsg) {
		Debug.Log("Client received pairing done message");
		var beginMessage = netMsg.ReadMessage<IntegerMessage>();
		if (beginMessage.value == NetworkMessageConstants.ARG_PAIRING_DONE) {
			waitPanel.SetActive (false);
		} else if (beginMessage.value == NetworkMessageConstants.ARG_PAIRING_END) {
			Debug.Log("Server ended pairing. Wait for new player to pair with.");
			waitPanel.SetActive (true);
			// Call OnClientConnect as we just connected to the server.
			OnClientConnect (netMsg);
		}
	}

	/*
	 * Button events
	*/

	public void OnLeftDown(BaseEventData data_) {
		PointerEventData data = (PointerEventData)data_;
		Debug.Log ("Left Down");
		handleNewDirection (data.pointerId, -1);
	}

	public void OnLeftUp(BaseEventData data_) {
		PointerEventData data = (PointerEventData)data_;
		Debug.Log ("Left Up");
		handleNewDirection (data.pointerId, 0);
	}

	public void OnRightDown(BaseEventData data_) {
		PointerEventData data = (PointerEventData)data_;
		Debug.Log ("Right Down");
		handleNewDirection (data.pointerId, 1);
	}

	public void OnRightUp(BaseEventData data_) {
		PointerEventData data = (PointerEventData)data_;
		Debug.Log ("Right Up");
		handleNewDirection (data.pointerId, 0);
	}

	/*
	 * Handle new direction
	*/
	private void handleNewDirection(int id, int d) {

		bool triggerServerEvent = false;

		if (d != 0) {
			/*
			 * new pointer down
			 * 
			 * prev = current
			 * current = new
			*/
			prev_pointer_id = current_pointer_id;
			prev_direction = current_direction;
			current_pointer_id = id;
			current_direction = d;
			triggerServerEvent = true;
		} else {
			// pointer up
			if (id == current_pointer_id) {
				/*
				 * current pointer released
				 * 
				 * current = prev
				 * prev = NULL
				*/
				current_pointer_id = prev_pointer_id;
				current_direction = prev_direction;
				prev_pointer_id = -1;
				prev_direction = 0;
				triggerServerEvent = true;
			} else if (id == prev_pointer_id) {
				/*
				 * prev pointer released 
				 * 
				 * prev = NULL
				*/
				prev_pointer_id = -1;
				prev_direction = 0;
			} else {
				/*
				 * old pointer released
				 * 
				 * do nothing
				*/
			}
		}
			
		if (triggerServerEvent) {
			IntegerMessage msg;
			switch (current_direction) {
			case -1:
				msg = new IntegerMessage (NetworkMessageConstants.ARG_MOVE_LEFT);
				break;
			case 0:
				msg = new IntegerMessage (NetworkMessageConstants.ARG_MOVE_STOP);
				break;
			case 1:
				msg = new IntegerMessage (NetworkMessageConstants.ARG_MOVE_RIGHT);
				break;
			default:
				msg = new IntegerMessage (NetworkMessageConstants.ARG_MOVE_STOP);
				break;
			}
			Debug.Log ("Client send " + current_direction);
			client.Send (NetworkMessageConstants.MSG_TYPE_MOVE, msg);
		}

	}

}
