﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextFade : MonoBehaviour {

    public bool StartFade;
    public float FadeTime;
    private float time;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        this.GetComponent<Image>().color = new Color(1, 1, 1, 0);
        if (StartFade)
        {
            time += Time.deltaTime;
            this.GetComponent<Image>().color = Color.Lerp(new Color(1, 1, 1, 0), Color.white, 1 - (time / FadeTime));
        }
	}
}
