﻿using UnityEngine;
using System.Collections;

public class Baby : MonoBehaviour {

    public bool Dying;
    public float FadeTime;
    private float time;

	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {

        if (Dying)
        {
            time += Time.deltaTime;
            
            this.GetComponent<SpriteRenderer>().color = Color.Lerp(new Color(1, 1, 1, 0), Color.white, 1-(time/FadeTime));
            if(time > FadeTime)
            {
                Destroy(this.gameObject);
            }
        }
	}

    public void Die()
    {
        Dying = true;
        this.GetComponent<Rigidbody2D>().drag = 1;
    }

    void OnDestroy()
    {
        var cam = Camera.main;
        if (cam == null)
            return;

        var camFitter = cam.GetComponent<FitCameraToGameObjects>();
        if (camFitter != null && camFitter.ObjectsToFit.Contains(this.gameObject))
            camFitter.ObjectsToFit.Remove(this.gameObject);

    }
}
