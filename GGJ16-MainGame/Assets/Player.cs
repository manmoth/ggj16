﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D), typeof(Animator))]
public class Player : MonoBehaviour
{
    public Animator Animator;
    public Rigidbody2D RigidBody;
	public GameObject nickPrototype;
	public GameObject nick;
    public int Score = 0;
    public string nameString = "";


    public Player otherPlayer;
	public SheetScaler sheetScaler;

    public Vector2 RunThreshold = new Vector2(0.1f, 1f);
    public Vector2 IdleThreshold = new Vector2(0.1f, 1f);
    public Vector2 CurrentMoveForce = new Vector2();
    public Vector2 ForceDirection { get; set; }
    public float Exhaustion;
    public float ExhaustionRate;
    public float RecoveryRate;

    public float DragTimeWithoutMove;
    public float DragTimeBeforeTug = 1f;

	void Awake() {
		nick = GameObject.Instantiate(nickPrototype, new Vector3(0, 0, -10) , Quaternion.identity) as GameObject;
		nick.transform.parent = transform;
	}

    // Use this for initialization
    void Start () {
        Animator = GetComponent<Animator>();
        RigidBody = GetComponent<Rigidbody2D>();
        Exhaustion = 1;

    }

    // Update is called once per frame
    void Update()
    {
        if (DragTimeWithoutMove > DragTimeBeforeTug)
            Animator.SetBool("Tug", true);
        else
            Animator.SetBool("Tug", false);

        if (Animator.GetBool("Run"))
            Exhaustion += Time.deltaTime * ExhaustionRate;
        else if (Exhaustion > 1)
            Exhaustion -= Time.deltaTime * RecoveryRate;

        Animator.SetFloat("Exhaustion", Exhaustion);

        if (RigidBody.velocity.magnitude >= IdleThreshold.magnitude)
            Animator.SetBool("Walk", true);
        else
        {
            Animator.SetBool("Walk", false);
        }
        if (RigidBody.velocity.magnitude >= RunThreshold.magnitude)
            Animator.SetBool("Run", true);
        else
        {
            Animator.SetBool("Run", false);
        }

        nick.transform.GetComponentInChildren<TextMesh>().text = nameString + " (" + Score + ")";
    }

}
