﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AI : MonoBehaviour {
    
    public enum AiState
    {
        WaitingForPlayers,
        StartingGame,
        PrepareLaunch,
        LaunchCountdown,
        Launch,
		BabyFlying,
        BabyDying,
		StartingOver,
		StartingOverFadingBack
    }

    AiState currentState;
	float timerCountDown;
	float faderWaitForPlayers;
	float faderStartOver;

    Wizard wizard;
	GameObject countDownGameObject;
    BabyLauncher launcher;
    NetworkManager networkManager;
	GameObject waitingScreen;
	GameObject startOverScreen;
    GameObject ghost;

    void Start () {
        wizard = GameObject.FindObjectOfType<Wizard>();
        countDownGameObject = GameObject.Find("Countdown");
        launcher = GameObject.FindObjectOfType<BabyLauncher>();
        currentState = AiState.WaitingForPlayers;
        networkManager = GameObject.FindObjectOfType<NetworkManager>();
		waitingScreen = GameObject.Find("WaitingForPlayersPanel");
		startOverScreen = GameObject.Find("StartOverPanel");
        faderWaitForPlayers = 1;
		faderStartOver = 0;
        ghost = GameObject.Find("GhostBaby");
    }
    
	
	void Update () {

        if (currentState == AiState.WaitingForPlayers)
        {
           
			/*
            Player p1, p2;
            PlayerController pc1 = networkManager.AddPlayer("Ballz", 1, out p1);
            PlayerController pc2 = networkManager.AddPlayer("Deep", 1, out p2);

            Camera.main.GetComponent<FitCameraToGameObjects>().ObjectsToFit.Add(p1.gameObject);
            Camera.main.GetComponent<FitCameraToGameObjects>().ObjectsToFit.Add(p2.gameObject);

            GameObject kIn = Instantiate(Resources.Load("KeyboardInput") as GameObject);
            kIn.GetComponent<KeyboardInput>().PlayerControllers.Add(pc1);
            GameObject kIn1 = Instantiate(Resources.Load("KeyboardInput") as GameObject);
            kIn1.GetComponent<KeyboardInput>().PlayerControllers.Add(pc2);
            kIn1.GetComponent<KeyboardInput>().LeftKey = KeyCode.LeftArrow;
            kIn1.GetComponent<KeyboardInput>().RightKey = KeyCode.RightArrow;
            currentState = AiState.StartingGame;
            */
            

			faderWaitForPlayers += Time.deltaTime;
			if (faderWaitForPlayers > 1) {
				faderWaitForPlayers = 1;
			}
			waitingScreen.GetComponent<CanvasGroup> ().alpha = faderWaitForPlayers;

			GameObject.FindObjectOfType<GameServer> ().PutAllWaitingPlayersIntoGame ();
			if (networkManager.ConnectedPlayers.Count > 0)
				currentState = AiState.StartingGame;
		} else if (currentState == AiState.StartingGame) {
			GameObject.FindObjectOfType<GameServer> ().PutAllWaitingPlayersIntoGame ();
			if (networkManager.ConnectedPlayers.Count == 0) {
				currentState = AiState.WaitingForPlayers;
				return;
			}
			faderWaitForPlayers -= Time.deltaTime;
			if (faderWaitForPlayers < 0) {
				faderWaitForPlayers = 0;
			}
			waitingScreen.GetComponent<CanvasGroup> ().alpha = faderWaitForPlayers;

			if ((faderWaitForPlayers <= 0) && (FindObjectsOfType<Baby> ().Length == 0))
				currentState = AiState.PrepareLaunch;
		} else if (currentState == AiState.PrepareLaunch) {
			GameObject.FindObjectOfType<GameServer> ().PutAllWaitingPlayersIntoGame ();
			wizard.GetComponent<Animator> ().SetBool ("BabyInBasket", true);
			launcher.LoadBaby ();
			Camera.main.GetComponent<FitCameraToGameObjects> ().ObjectsToFit.Add (GameObject.FindObjectOfType<BabyLauncher> ().gameObject);

			timerCountDown = 3;
			currentState = AiState.LaunchCountdown;
		} else if (currentState == AiState.LaunchCountdown) {
			timerCountDown -= Time.deltaTime;
			if (timerCountDown <= 0) {
				currentState = AiState.Launch;
				countDownGameObject.transform.FindChild ("Go").GetComponent<TextFade> ().StartFade = true;
			} else if (timerCountDown <= 1) {
				wizard.GetComponent<Animator> ().SetBool ("Fire", true);
				countDownGameObject.transform.FindChild ("1").GetComponent<TextFade> ().StartFade = true;
			} else if (timerCountDown <= 2) {
				wizard.GetComponent<Animator> ().SetBool ("Fire", true);
				countDownGameObject.transform.FindChild ("2").GetComponent<TextFade> ().StartFade = true;
			} else if (timerCountDown <= 3) {
				countDownGameObject.transform.FindChild ("3").GetComponent<TextFade> ().StartFade = true;
			}
		} else if (currentState == AiState.Launch) {
			launcher.LaunchBaby ();
			Camera.main.GetComponent<FitCameraToGameObjects> ().ObjectsToFit.Remove (GameObject.FindObjectOfType<BabyLauncher> ().gameObject);
			currentState = AiState.BabyFlying;
		} else if (currentState == AiState.BabyFlying) {
			Baby baby = GameObject.FindObjectOfType<Baby>();
			if (baby != null) {
				if (baby.Dying) {
                    currentState = AiState.BabyDying;
                    timerCountDown = 3;
                    ghost.transform.position = baby.transform.position;
                    ghost.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                    ghost.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 1000));
                    ghost.GetComponent<SpriteRenderer>().color = Color.white;

                }
			}
		} 
		else if (currentState == AiState.BabyDying) {
			timerCountDown -= Time.deltaTime;
            ghost.GetComponent<SpriteRenderer>().color = Color.Lerp(new Color(1, 1, 1, 0), Color.white, (timerCountDown / 3));

			if (timerCountDown <= 0) {
				currentState = AiState.StartingOver;
				timerCountDown = 5;
                ghost.transform.position = new Vector2(-1000, 0);

            }
		} else if (currentState == AiState.StartingOver) {
			if (timerCountDown == 5) {
				GameServer gs = GameObject.FindObjectOfType<GameServer>();
				gs.DetonateNuke ();
				//foreach (SheetScaler ss in GameObject.FindObjectsOfType<SheetScaler>()) {
				//	Vector3 pos = ss.gameObject.transform.position;
				//	pos.x = 0;
				//	ss.gameObject.GetComponent<Rigidbody2D>().position = pos;
				//	ss.PlayerA?.transform.position = pos;
				//	ss.PlayerB?.transform.position = pos;
				//}
			}
			timerCountDown -= Time.deltaTime;

			faderStartOver += Time.deltaTime;
			if (faderStartOver >= 1) {
				faderStartOver = 1;
			}
			startOverScreen.GetComponent<CanvasGroup> ().alpha = faderStartOver;


			if (timerCountDown <= 0) {
				currentState = AiState.StartingOverFadingBack;
			}
		}
		else if (currentState == AiState.StartingOverFadingBack) {

			faderStartOver -= Time.deltaTime;
			if (faderStartOver <= 0) {
				faderStartOver = 0;
			}
			startOverScreen.GetComponent<CanvasGroup> ().alpha = faderStartOver;


			if (faderStartOver <= 0) {
				currentState = AiState.StartingGame;
			}
		}
	}
}
