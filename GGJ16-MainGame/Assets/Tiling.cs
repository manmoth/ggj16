﻿using UnityEngine;
using System.Collections;

public class Tiling : MonoBehaviour {

    public Vector3 StartPosition;

    GameObject[] TilePrototypes;
    
    public GameObject[] TopTiles;
    public GameObject[,] BottomTiles;
    
    private Transform groundParent;

	// Use this for initialization
	void Start ()
    {
        TopTiles = new GameObject[500];
        BottomTiles = new GameObject[500,2];

        TilePrototypes = new GameObject[6];
        TilePrototypes[0] = Resources.Load("Background1") as GameObject;
        TilePrototypes[1] = Resources.Load("Background2") as GameObject;
        TilePrototypes[2] = Resources.Load("Background3") as GameObject;
        TilePrototypes[3] = Resources.Load("Background4") as GameObject;
        TilePrototypes[4] = Resources.Load("Ground1") as GameObject;
        TilePrototypes[5] = Resources.Load("Ground2") as GameObject;

        Vector2 protobounds = TilePrototypes[0].GetComponent<SpriteRenderer>().bounds.size;
        for(int i = 0; i < TopTiles.Length; i++)
        {
            Vector3 tilePos = StartPosition + new Vector3(protobounds.x * i, 0);
            GameObject tile = Instantiate(TilePrototypes[Random.Range(0, 4)], tilePos, Quaternion.identity) as GameObject;
            TopTiles[i] = tile;
        }
        for(int i = 0; i < BottomTiles.GetLength(0); i++)
        {
            for (int j = 0; j < BottomTiles.GetLength(1); j++)
            {
                Vector3 tilePos = StartPosition + new Vector3(protobounds.x * i, -protobounds.y*(1+j));
                GameObject tile = Instantiate(TilePrototypes[(Random.Range(0f, 1f) > 0.1f ? 4 : 5)], tilePos, Quaternion.identity) as GameObject;
                BottomTiles[i,j] = tile;
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
	}
}
