﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SheetScaler : MonoBehaviour {

    public float MaxXScale;
    public float MinXScale;

    public Player PlayerA;
    public Player PlayerB;
    public Vector2 ObjectAOffset;
    public Vector2 ObjectBOffset;

    public DistanceJoint2D DistJointA;
    public DistanceJoint2D DistJointB;
    public HingeJoint2D JointA;
    public HingeJoint2D JointB;

    int score = 0;

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Baby"))
        {
            PlayerA.Score += 1;
            PlayerB.Score += 1;
            var forceVector = new Vector2(Random.Range(0f, 0.1f), Random.Range(0.75f, 1f)).normalized;
            other.gameObject.GetComponent<Rigidbody2D>().AddForce(forceVector * Random.Range(3f, 5f) * 5, ForceMode2D.Impulse);
        }
    }

    // Use this for initialization
    void Start () {
    }

    public void Connect()
    {
        PlayerA.GetComponent<SpriteRenderer>().flipX = false;
        PlayerA.transform.position = new Vector3(this.transform.position.x - ObjectAOffset.x, PlayerB.transform.position.y, 0);
        PlayerB.GetComponent<SpriteRenderer>().flipX = true;
        PlayerB.transform.position = new Vector3(this.transform.position.x - ObjectBOffset.x - PlayerB.GetComponent<BoxCollider2D>().bounds.extents.x, PlayerB.transform.position.y, 0);

        JointA = this.gameObject.AddComponent<HingeJoint2D>();
        JointB = this.gameObject.AddComponent<HingeJoint2D>();

        JointA.connectedBody = PlayerA.GetComponent<Rigidbody2D>();
        JointA.anchor = new Vector2(-GetComponent<BoxCollider2D>().bounds.extents.x, 0);
        JointA.connectedAnchor = ObjectAOffset;
        JointB.connectedBody = PlayerB.GetComponent<Rigidbody2D>();
        JointB.anchor = new Vector2(GetComponent<BoxCollider2D>().bounds.extents.x, 0);
        JointB.connectedAnchor = ObjectBOffset;
    }

    // Update is called once per frame
    void Update ()
    {
        // Todos: Total score. Round score (bounces). Fade to black and show round score before restarting. Move players to initial position.
        if(PlayerA != null && PlayerB != null)
        {
            if(PlayerA.ForceDirection.magnitude > 0 || PlayerB.ForceDirection.magnitude > 0)
            {
                float scaleIncrease = 0;
                if (Mathf.Abs(PlayerA.ForceDirection.x) != Mathf.Abs(PlayerB.ForceDirection.x))
                {
                    if (PlayerA.ForceDirection.x == -1)
                        scaleIncrease = 0.1f;
                    else if(PlayerA.ForceDirection.x == 1)
                        scaleIncrease = -0.1f;

                    if (PlayerB.ForceDirection.x == -1)
                        scaleIncrease = -0.1f;
                    else if (PlayerB.ForceDirection.x == 1)
                        scaleIncrease = 0.1f;
                }
                if (Mathf.Abs(PlayerA.ForceDirection.x) == Mathf.Abs(PlayerB.ForceDirection.x) && 
                    Mathf.Sign(PlayerA.ForceDirection.x) != Mathf.Sign(PlayerB.ForceDirection.x))
                {
                    if (PlayerA.ForceDirection.x > PlayerB.ForceDirection.x)
                        scaleIncrease = 0.1f;
                    else
                        scaleIncrease = -0.1f;
                }

                Vector3 scale = this.transform.localScale;
                if (scale.x+scaleIncrease > MinXScale && scale.x+scaleIncrease < MaxXScale)
                {
                    scale.x += scaleIncrease;
                    this.transform.localScale = scale;
                }
            }
        }
    }
}
