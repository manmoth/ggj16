﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Player))]
public class PlayerController : MonoBehaviour {

    public Vector2 MoveForce = new Vector2(750, 0);
    public Vector2 MaxVelocity;

    private Player player;
	public int connectionId;

    // Use this for initialization
    void Start ()
    {
        player = GetComponent<Player>();
    }
	
	// Update is called once per frame
	void Update () {
		if (player.RigidBody != null)
        {
            if (player.ForceDirection.magnitude > 0 && player.RigidBody.velocity.magnitude < 5f)
                player.DragTimeWithoutMove += Time.deltaTime;
            else
                player.DragTimeWithoutMove = 0;

            player.CurrentMoveForce.x = MoveForce.x * player.ForceDirection.x;
            player.CurrentMoveForce.y = MoveForce.y * player.ForceDirection.y;

            if(player.RigidBody.velocity.magnitude < MaxVelocity.magnitude)
                player.RigidBody.AddForce(player.CurrentMoveForce, ForceMode2D.Force);
        }
    }

	public void OnReceivedCommand(int direction) {
		switch (direction) {
		case -1:
			MoveLeft ();
			break;
		case 0:
			StopMoving ();
			break;
		case 1:
			MoveRight ();
			break;
		default:
			StopMoving ();
			break;
		}
	}

	public Player GetPlayer() {
		return player;
	}

    public void MoveRight()
    {
        player.ForceDirection = new Vector2(1, 0);
        player.RigidBody.mass = 1;
    }

    public void MoveLeft()
    {
        player.ForceDirection = new Vector2(-1, 0);
        player.RigidBody.mass = 1;
    }

    public void StopMoving()
    {
        player.ForceDirection = Vector2.zero;
        player.RigidBody.mass = 100;
    }
}
