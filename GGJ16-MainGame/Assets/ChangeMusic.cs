﻿using UnityEngine;
using System.Collections;

public class ChangeMusic : MonoBehaviour
{
    public AudioClip level1Music;
    public AudioClip menuMusic;
    private AudioSource source;

    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    void OnLevelWasLoaded(int level)
    {
        if (level == 1)
        {
            source.clip = level1Music;
            source.Play();
        }
        else if (level == 0)
        {
            if (source.clip == level1Music)
            {
                source.clip = menuMusic;
                source.Play();
            }
        }

    }
}