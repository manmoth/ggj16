﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KeyboardInput : MonoBehaviour {

    public KeyCode LeftKey;
    public KeyCode RightKey;
    public List<PlayerController> PlayerControllers = new List<PlayerController>();

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	    foreach(PlayerController pk in PlayerControllers)
        {
            if (Input.GetKeyDown(LeftKey))
                pk.MoveLeft();
            if (Input.GetKeyDown(RightKey))
                pk.MoveRight();

            if (!Input.GetKey(LeftKey) && !Input.GetKey(RightKey))
                pk.StopMoving();
        }
	}
}
