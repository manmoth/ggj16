﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Button))]
public class Mute : MonoBehaviour {

    bool isMute;
    public Sprite newSprite;
    private GameObject btnObject;

    void Start()
    {
        btnObject = GameObject.Find("MuteButton");
    }

    void Update()
    {
        if (!isMute)
            btnObject.GetComponent<Button>().image.overrideSprite = newSprite;
        else
            btnObject.GetComponent<Button>().image.overrideSprite = null;
    }

    public void MuteAudio()
    {
        isMute = !isMute;
        AudioListener.volume = isMute ? 0 : 1;
    }
}
