﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class NetworkManager : MonoBehaviour {
    public Vector2 PlayerStartPos;

    GameObject playerPrototype;
	GameObject sheetPrototype;

    public Dictionary<Player, Player> ConnectedPlayers = new Dictionary<Player, Player>();

    // Use this for initialization
    void Start () {
        playerPrototype = Resources.Load("Player") as GameObject;
		sheetPrototype = Resources.Load("Sheet") as GameObject;
    }

	public PlayerController AddPlayer(string nick, int id, out Player p)
    {
		Debug.Log ("AddPlayer: " + nick + " ID: " + id);
        GameObject player = GameObject.Instantiate(playerPrototype, PlayerStartPos, Quaternion.identity) as GameObject;
		p = player.GetComponent<Player> ();
        p.nameString = nick;
		PlayerController ctrl = player.GetComponent<PlayerController>();
		ctrl.connectionId = id;

		Camera.main.GetComponent<FitCameraToGameObjects> ().ObjectsToFit.Add (p.gameObject);

		p.nick.transform.GetComponentInChildren<TextMesh> ().text = nick;
        p.nick.transform.position = new Vector3(0, -4, 0);
		return ctrl;
    }

	public int HandlePlayerDisconnect(Player player) {
		Player other = player.otherPlayer;
		SheetScaler sheetScaler = player.sheetScaler;
		PlayerController otherCtrl = other.GetComponent<PlayerController>();
		int otherId = otherCtrl.connectionId;

		ConnectedPlayers.Remove (player);
		ConnectedPlayers.Remove (other);

		Camera.main.GetComponent<FitCameraToGameObjects> ().ObjectsToFit.Remove (player.gameObject);
		Camera.main.GetComponent<FitCameraToGameObjects> ().ObjectsToFit.Remove (other.gameObject);

		Destroy (sheetScaler.gameObject);
		Destroy (other.gameObject);
		Destroy (player.gameObject);

		return otherId;
	}

    public void ConnectPlayers()
    {
        List<Player> unconnected = new List<Player>();

        Player[] players = GameObject.FindObjectsOfType<Player>();
        foreach(Player player in players)
        {
            if(!ConnectedPlayers.ContainsKey(player))
                unconnected.Add(player);
        }

        if(unconnected.Count > 1)
        {
			
            Player p1 = unconnected[0];
            Player p2 = unconnected[1];
            ConnectedPlayers[p1] = p2;
            ConnectedPlayers[p2] = p1;

			p1.otherPlayer = p2;
			p2.otherPlayer = p1;

            GameObject sheet = GameObject.Instantiate(sheetPrototype, PlayerStartPos, Quaternion.identity) as GameObject;
            sheet.GetComponent<SheetScaler>().PlayerA = p1;
            sheet.GetComponent<SheetScaler>().PlayerB = p2;
            sheet.GetComponent<SheetScaler>().Connect();

			SheetScaler sheetScaler = sheet.GetComponent<SheetScaler> ();

			p1.sheetScaler = sheetScaler;
			p2.sheetScaler = sheetScaler;

        }
    }

    // Update is called once per frame
    void Update () {
	
	}
}
