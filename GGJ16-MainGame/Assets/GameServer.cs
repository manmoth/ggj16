﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class GameServer : MonoBehaviour {

	Dictionary<int, PlayerController> playerControls;
	Dictionary<int, string> nicks;

	NetworkManager networkManager;

	Queue<int> waitingPlayers;

	// Use this for initialization
	void Start () {
		waitingPlayers = new Queue<int> ();
		networkManager = GameObject.FindObjectOfType<NetworkManager> ();
		playerControls = new Dictionary<int, PlayerController> ();
		nicks = new Dictionary<int, string> ();
		DontDestroyOnLoad (transform.gameObject);
		StartServer (8487);
	}

	// Update is called once per frame
	void Update () {
	}

	public void PutAllWaitingPlayersIntoGame() {
		while (waitingPlayers.Count >= 2) {

			int id1 = waitingPlayers.Dequeue ();
			int id2 = waitingPlayers.Dequeue ();

			string nick1 = "Baby" + Random.Range(1000, 9999);
			string nick2 = "Baby" + Random.Range(1000, 9999);

			if (nicks.ContainsKey (id1)) {
				nick1 = nicks[id1];
			}
			if (nicks.ContainsKey (id2)) {
				nick2 = nicks[id2];
			}

			Debug.Log ("Nick1: " + nick1);
			Debug.Log ("Nick2: " + nick2);

            Player p1, p2;
			PlayerController pc1 = networkManager.AddPlayer (nick1, id1, out p1);
			PlayerController pc2 = networkManager.AddPlayer (nick2, id2, out p2);

			playerControls.Add (id1, pc1);
			playerControls.Add (id2, pc2);

			IntegerMessage msg = new IntegerMessage (NetworkMessageConstants.ARG_PAIRING_DONE);
			NetworkServer.connections[id1].Send (NetworkMessageConstants.MSG_TYPE_PAIRING, msg);
			NetworkServer.connections[id2].Send (NetworkMessageConstants.MSG_TYPE_PAIRING, msg);

		}
		networkManager.ConnectPlayers ();
	}

	public void StartServer(int port) {
		if (NetworkServer.active)
		{
			Debug.LogError("Server already running. Will not start again.");
			return;
		}

		Debug.Log ("NetworkServer starts listening on port " + port);
		NetworkServer.Listen(port);

		// system msgs
		NetworkServer.RegisterHandler(MsgType.Connect, OnClientConnect);
		NetworkServer.RegisterHandler(MsgType.Disconnect, OnClientDisconnect);
		NetworkServer.RegisterHandler(MsgType.Error, OnServerError);

		NetworkServer.RegisterHandler(NetworkMessageConstants.MSG_TYPE_MOVE, OnClientMoveMessage);
		NetworkServer.RegisterHandler(NetworkMessageConstants.MSG_TYPE_NICK, OnClientNickMessage);

		DontDestroyOnLoad(gameObject);	
	}

	public void ResetServer()
	{
		NetworkServer.Shutdown();
	}

	void OnClientConnect(NetworkMessage netMsg)
	{
		Debug.Log("New client connected to GameServer. ID: " + netMsg.conn.connectionId);
	}

	public void DetonateNuke() {
		//bool t = false;
		//foreach (NetworkConnection c in NetworkServer.connections) {
		//}
		//foreach (NetworkConnection c in NetworkServer.connections) {
		//	if (c != null) {
		//		DoDisStuff (c.connectionId);
		//	}
		//}
	}

	void OnClientDisconnect(NetworkMessage netMsg)
	{
		DoDisStuff (netMsg.conn.connectionId);
	}

	void DoDisStuff(int id) {

		Debug.Log("Connection lost from client. ID: " + id);

		Player player = playerControls[id].GetPlayer();
		int otherId = networkManager.HandlePlayerDisconnect (player);

		Debug.Log ("OtherID from HandlePlayerDisconnect(): " + otherId);

		IntegerMessage msg = new IntegerMessage (NetworkMessageConstants.ARG_PAIRING_END);
		NetworkServer.connections[otherId].Send (NetworkMessageConstants.MSG_TYPE_PAIRING, msg);

		nicks.Remove(id);
		nicks.Remove(otherId);

		playerControls.Remove (id);
		playerControls.Remove (otherId);

		// TODO: If user disconnects when in the queue, then undefined behaviour 
		//Queue<int> waitingPlayers;

	}

	void OnServerError(NetworkMessage netMsg)
	{
		Debug.LogError("ServerError from Master. ID: " + netMsg.conn.connectionId);
	}

	void OnClientMoveMessage(NetworkMessage netMsg)
	{
		var beginMessage = netMsg.ReadMessage<IntegerMessage>();
		int direction = 0;
		switch (beginMessage.value) {
		case NetworkMessageConstants.ARG_MOVE_STOP:
			direction = 0;
			break;
		case NetworkMessageConstants.ARG_MOVE_LEFT:
			direction = -1;
			break;
		case NetworkMessageConstants.ARG_MOVE_RIGHT:
			direction = 1;
			break;
		default:
			direction = 0;
			break;
		}
		Debug.Log("received OnClientMoveMessage: " + beginMessage.value + " from ID: " + netMsg.conn.connectionId);
		if (playerControls.ContainsKey (netMsg.conn.connectionId)) {
			playerControls [netMsg.conn.connectionId].OnReceivedCommand(direction);
		} else {
			Debug.LogError ("connectionId is not in playerControls");
		}
	}

	void OnClientNickMessage(NetworkMessage netMsg) {
		var beginMessage = netMsg.ReadMessage<StringMessage>();
		string nick = beginMessage.value;
		nicks.Add (netMsg.conn.connectionId, nick);
		waitingPlayers.Enqueue (netMsg.conn.connectionId);
		Debug.Log("uId " + netMsg.conn.connectionId + " uses the nick '" + nick + "'");
	}
}
