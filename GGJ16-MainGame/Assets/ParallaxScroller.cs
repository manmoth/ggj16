﻿using UnityEngine;
using System.Collections.Generic;

public class ParallaxScroller : MonoBehaviour {

    public float SpeedFactor = 0.001f;
    public List<GameObject> Layers;
    public List<float> Speeds;

    float xOffset;

	// Use this for initialization
	void Start () {
        xOffset = gameObject.transform.position.x;
	}
	
	// Update is called once per frame
	void Update ()
    {
        //For testing
        //Vector3 pos = Camera.main.transform.position;
        //pos.x += Time.deltaTime * 4;
        //Camera.main.transform.position = pos;

        float camX = Camera.main.transform.position.x;
        foreach (GameObject layer in Layers)
        {
            Vector3 layerPos = layer.transform.position;
            layerPos.x -= Speeds[Layers.IndexOf(layer)] * SpeedFactor;
            layer.transform.position = layerPos;
        }
    }
}
