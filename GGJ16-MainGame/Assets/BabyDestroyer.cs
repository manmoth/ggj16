﻿using UnityEngine;
using System.Collections.Generic;

public class BabyDestroyer : MonoBehaviour {

    public bool DestroyAll = true;
    public List<string> TagsToDestroy = new List<string>{ "Baby" };

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (DestroyAll || TagsToDestroy.Contains(other.tag))
        {
            if(other.gameObject.tag == "Baby")
                other.gameObject.GetComponent<Baby>().Die();
        }
    }
}
