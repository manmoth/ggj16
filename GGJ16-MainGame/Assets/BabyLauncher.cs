﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class BabyLauncher : MonoBehaviour
{

    private enum State { PrepareLoad, Loading, PrepareLaunch, Launch }
    private State state = State.PrepareLoad;

    public GameObject BabyPrefab;
    public GameObject CatapultObject;
    public GameObject CatapultArmObject;
    public GameObject BabySpawnPosition;
    // Launch force for babies
    public float MinForce = 10;
    public float MaxForce = 50;
    // Rotation presets
    public float MinArmRotation = 37;
    public float MaxArmRotation = 320;
    public float LoadingArmRotation = 0;
    public float AcceptableInaccuracy = 1f;
    // Rotation speeds
    public float LoadingArmRotationSpeed = 10;
    public float PrepareLaunchArmRotationSpeed = 8;
    public float ReleaseArmRotationSpeed = 5;
    public float AfterLaunchArmRotationSpeed = 0;
    public float JerkyInfluence = 10;
    // The different babies
    public List<Sprite> BabySprites;

    private GameObject loadedBabyObject = null;
    float currentLerpTime = 0;
    private float targetArmAngle = 0;
    private float currentRotationSpeed = 15;                // Degrees per second
    private bool loadBabyWhenReady = false;
    private bool launchBabyWhenReady = false;
    private State previousState = State.PrepareLoad;

    void Awake()
    {
        // Hide helper object. (Use to set the transform of spawned babies.)
        BabySpawnPosition.SetActive(false);
    }

    // Use this for initialization
    void Start()
    {

    }

    bool IsStateChanged()
    {
        return state != previousState;
    }

    /// <summary>
    /// Ready to shoot baby into air, i.e. arm at minimum potential.
    /// </summary>
    /// <param name="currentZRotation"></param>
    /// <returns></returns>
    bool IsArmReadyToLaunch(float currentZRotation)
    {
        return Mathf.Abs(MaxArmRotation - currentZRotation) < AcceptableInaccuracy;
    }

    /// <summary>
    /// Ready to release the arm into movement, i.e. arm at max potential.
    /// </summary>
    /// <param name="currentZRotation"></param>
    /// <returns></returns>
    bool IsArmReadyToRelease(float currentZRotation)
    {
        return Mathf.Abs(MinArmRotation - currentZRotation) < AcceptableInaccuracy;
    }

    /// <summary>
    /// Arm is at a convenient level for placing babies in it.
    /// </summary>
    /// <param name="currentZRotation"></param>
    /// <returns></returns>
    bool IsArmReadyToLoad(float currentZRotation)
    {
        return Mathf.Abs(LoadingArmRotation - currentZRotation) < AcceptableInaccuracy;
    }

    // Update is called once per frame
    void Update()
    {
        bool stateChange = IsStateChanged();
        if (stateChange)
            currentLerpTime = 0;
        currentLerpTime += Time.deltaTime;
        var currentRotation = CatapultArmObject.transform.rotation.eulerAngles.z;
        var jerkyTime = currentLerpTime;

        switch (state)
        {
            case State.PrepareLoad:
                currentRotationSpeed = LoadingArmRotationSpeed + JerkyInfluence * Mathf.Sin(jerkyTime);
                targetArmAngle = LoadingArmRotation;
                if (IsArmReadyToLoad(currentRotation) && loadBabyWhenReady)
                    state = State.Loading;
                break;
            case State.Loading:
                if (IsArmReadyToLoad(currentRotation) && loadBabyWhenReady)
                    _LoadBaby();
                else
                    state = State.PrepareLoad;
                break;
            case State.PrepareLaunch:
                currentRotationSpeed = PrepareLaunchArmRotationSpeed + JerkyInfluence * Mathf.Sin(jerkyTime);
                targetArmAngle = MinArmRotation;
                if (IsArmReadyToRelease(currentRotation) && launchBabyWhenReady)
                    state = State.Launch;
                break;
            case State.Launch:
                currentRotationSpeed = ReleaseArmRotationSpeed;
                targetArmAngle = MaxArmRotation;
                if (IsArmReadyToLaunch(currentRotation) && launchBabyWhenReady)
                    _LaunchBaby();
                break;
        }

        var totalTime = Mathf.Abs(targetArmAngle - CatapultArmObject.transform.rotation.eulerAngles.z) / currentRotationSpeed;
        var t = Mathf.Clamp01(currentLerpTime / totalTime);
        //if (state == State.Launch)
        //    t = t * t;
        var newZrotation = Mathf.LerpAngle(CatapultArmObject.transform.rotation.eulerAngles.z, targetArmAngle, t);
        CatapultArmObject.transform.rotation = Quaternion.Euler(0, 0, newZrotation);
        previousState = state;
    }

    Sprite GetRandomSprite()
    {
        int i = Random.Range(0, BabySprites.Count);
        return BabySprites[i];
    }

    public void LoadBaby()
    {
        Debug.Log("Preparing to load, and will then load baby.");
        loadBabyWhenReady = true;
        state = State.PrepareLoad;
    }

	public void LaunchBaby()
    {
        if (!IsBabyLoaded())
        {
            Debug.Log("Load baby before launching.");
        }
        else
        {
            Debug.Log("Preparing to launch, and will then launch.");
            launchBabyWhenReady = true;
            state = State.PrepareLaunch;
        }
    }

    public void PrepareLoad()
    {
        Debug.Log("Prepare to load baby!");
        state = State.PrepareLoad;
    }

    public void PrepareLaunch()
    {
        Debug.Log("Prepare to launch");
        state = State.PrepareLaunch;
    }

    public bool IsBabyLoaded()
    {
        return loadedBabyObject != null;
    }

    void _LoadBaby()
    {
        if (IsBabyLoaded())
        {
            Debug.Log("There is already a baby in the launcher.");
            return;
        }
        Debug.Log("Loading a baby into launcher.");
        var posistion = BabySpawnPosition.transform.position;
        var rotation = BabySpawnPosition.transform.rotation;
        loadedBabyObject = Instantiate(BabyPrefab, posistion, rotation) as GameObject;
        loadedBabyObject.transform.localScale = BabySpawnPosition.transform.localScale;
        loadedBabyObject.GetComponent<Rigidbody2D>().isKinematic = true;
        loadedBabyObject.transform.parent = CatapultArmObject.transform;

        var renderer = loadedBabyObject.GetComponent<SpriteRenderer>();
        renderer.sprite = GetRandomSprite();
        var cameraFitter = Camera.main.GetComponent<FitCameraToGameObjects>();
        //if (cameraFitter != null)
        cameraFitter.ObjectsToFit.Add(loadedBabyObject);
        loadBabyWhenReady = false;
    }

    void _LaunchBaby()
    {
        if (!IsBabyLoaded())
        {
            Debug.Log("No baby to launch. (Load a baby in the launcher first.)");
            return;
        }
        Debug.Log("Launch the baby!");
        loadedBabyObject.transform.parent = null;
        float xForce = Random.Range(0.5f, 1);
        float yForce = Random.Range(0.5f, 1);
        var rb = loadedBabyObject.GetComponent<Rigidbody2D>();
        rb.isKinematic = false;
        rb.AddForce(new Vector2(xForce, yForce).normalized * Random.Range(MinForce, MaxForce), ForceMode2D.Impulse);
        loadedBabyObject = null;
        launchBabyWhenReady = false;
	}
}
