﻿using UnityEngine;
using System.Collections;

public class DontDelete : MonoBehaviour {

    public static DontDelete Instance;
    void Awake()
    {
        if(Instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
            
    }
}
