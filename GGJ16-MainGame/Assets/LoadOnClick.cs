﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadOnClick : MonoBehaviour {

    public GameObject loadingImage;

    public void LoadScene(int level)
    {
        if (level !=5)
        {
            loadingImage.SetActive(true);
            SceneManager.LoadScene(level);
        }
        else if (level == 5)
        {
            Application.Quit();
        }
    }
}
