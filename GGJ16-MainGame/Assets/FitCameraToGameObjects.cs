﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class FitCameraToGameObjects : MonoBehaviour
{
    public bool DebugBoundedBox = true;
    public bool ScaleCamera = true;
    public float ZPosition = -10f;
    public float DampingTime = 0.15f;
    public float Margin = 10f;
    public float MinScale = 0.5f;
    public float MaxScale = 200f;
    public GameObject CameraLimits;
    public Rect MovementIgnoredWithinRect = new Rect(0.5f, 0.5f, 0.2f, 0.2f);
    public GameObject Target = null;
    public List<GameObject> ObjectsToFit;

    private Vector3 velocity = Vector3.zero;
    private float zoom = 0;

    void Awake()
    {
    }


    // Use this for initialization
    void Start()
    {
    }

    Vector2 DistanceInViewport(Rect bbox)
    {
        var vpLowerLeft = Camera.main.WorldToViewportPoint(new Vector2(bbox.xMin, bbox.yMin));
        var vpUpperRight = Camera.main.WorldToViewportPoint(new Vector2(bbox.xMax, bbox.yMax));
        //var diff = vpUpperRight - vpLowerLeft;
        return (vpUpperRight - vpLowerLeft);
    }

    Rect GetObjectsBounds(out bool boundsHaveNaN)
    {
        boundsHaveNaN = false;
        var leftLimit = Mathf.Infinity;
        var rightLimit = Mathf.NegativeInfinity;
        var bottomLimit = Mathf.Infinity;
        var topLimit = Mathf.NegativeInfinity;
        for (int i = 0; i < ObjectsToFit.Count; i++)
        {
            var obj = ObjectsToFit[i];
            if (obj == null)
                continue;
            var renderer = obj.GetComponent<SpriteRenderer>();
            if (renderer != null)
            {
                var minX = obj.GetComponent<SpriteRenderer>().bounds.min.x;
                var maxX = obj.GetComponent<SpriteRenderer>().bounds.max.x;
                var minY = obj.GetComponent<SpriteRenderer>().bounds.min.y;
                var maxY = obj.GetComponent<SpriteRenderer>().bounds.max.y;
                leftLimit = minX < leftLimit ? minX : leftLimit;
                bottomLimit = minY < bottomLimit ? minY : bottomLimit;
                rightLimit = maxX > rightLimit ? maxX : rightLimit;
                topLimit = maxY > topLimit ? maxY : topLimit;
            }
            else
            {
                var x = obj.transform.position.x;
                var y = obj.transform.position.y;
                leftLimit = x < leftLimit ? x : leftLimit;
                bottomLimit = y < bottomLimit ? y : bottomLimit;
                rightLimit = x > rightLimit ? x : rightLimit;
                topLimit = y > topLimit ? y : topLimit;
            }
        }
        var values = new float[] { topLimit, bottomLimit, leftLimit, rightLimit };
        foreach (float value in values)
            if (float.IsNaN(value))
                boundsHaveNaN = true;
        return Rect.MinMaxRect(leftLimit - Margin, bottomLimit - Margin, rightLimit + Margin, topLimit + Margin);
    }

    // Update is called once per frame
    void Update()
    {
        if (ObjectsToFit.Count < 1)
        {
            //Debug.Log("No objects to follow.");
            return;
        }

        // Calculate orthographic camera bounds
        float viewHeight = 2f * Camera.main.orthographicSize;
        float viewWidth = viewHeight * Camera.main.aspect;

        // Object cluster center
        bool hasNaN;
        var boundingBox = GetObjectsBounds(out hasNaN);
        if (hasNaN)
        {
            Debug.Log("Bounding box contains NaN.");
            return;
        }
        if (DebugBoundedBox)
            DrawBoundingBox(boundingBox);
        var objectsCenter = boundingBox.center; // new Vector3((boundingBox.xMin + boundingBox.xMax) / 2, (boundingBox.yMin + boundingBox.yMax) / 2, ZPosition);
        if (Target != null)
            objectsCenter = Target.transform.position;

        // Events
        if (Input.GetKey(KeyCode.KeypadPlus))
            zoom -= .1f;
        if (Input.GetKey(KeyCode.KeypadMinus))
            zoom += .1f;

        if (Input.GetKey(KeyCode.KeypadPeriod))
            ScaleCamera = !ScaleCamera;

        // Translate
        //var vpObjectsCenter = Camera.main.WorldToViewportPoint(objectsCenter);
        //if (MovementIgnoredWithinRect.Contains(vpObjectsCenter))
        //{
        //    Debug.Log("Movement ignored");
        //}
        //else
        //{
        //var currentPosition = transform.position; //Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, ZPosition));

        // Find destination
        Vector3 delta = (Vector3)objectsCenter - transform.position;
        Vector3 destination = transform.position + delta;
        destination = new Vector3(destination.x, destination.y, ZPosition);

        // Constrain destination
        var xLim = CameraLimits.transform.position.x + viewWidth / 2;
        var yLim = CameraLimits.transform.position.y + viewHeight / 2;
        if (destination.x < xLim)                                           // Too far left
            destination.Set(xLim, destination.y, destination.z);            // Set x
        if (destination.y < yLim)                                           // Too low
            destination.Set(destination.x, yLim, destination.z);            // Set y

        // Move smoothly to destination
        transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, DampingTime);
        //transform.position.Set(100, 100, -10);
        //}

        // Scale viewport/camera
        bool targetInView = destination.sqrMagnitude < Mathf.Min(viewHeight * viewHeight, viewWidth * viewWidth);
        if (ScaleCamera)
        {
            var vpCamSize = Camera.main.orthographicSize;
            var vpDistance = DistanceInViewport(boundingBox);                               // Viewport distance
            var vpRatio = Mathf.Max(vpDistance.x, vpDistance.y);
            //var vpRatio = Mathf.Max(vpDistance.x / vpCamSize, vpDistance.y / vpCamSize);

            //var vpRatio = DistanceInViewport(boundingBox);                                    // Viewport distance
            Camera.main.orthographicSize = Mathf.Lerp(vpCamSize, vpCamSize * vpRatio + zoom, DampingTime);
            Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, MinScale, MaxScale);
        }
    }

    private void DrawBoundingBox(Rect boundingBox)
    {
        var p1 = new Vector2(boundingBox.xMin, boundingBox.yMin); // 00
        var p2 = new Vector2(boundingBox.xMax, boundingBox.yMin); // 10
        var p3 = new Vector2(boundingBox.xMax, boundingBox.yMax); // 11
        var p4 = new Vector2(boundingBox.xMin, boundingBox.yMax); // 01
        Debug.DrawLine(p1, p2);
        Debug.DrawLine(p2, p3);
        Debug.DrawLine(p3, p4);
        Debug.DrawLine(p4, p1);
    }
}

